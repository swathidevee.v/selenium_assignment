package com.assignment;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemowebShop1 {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		driver.get("https://demowebshop.tricentis.com/");
		driver.findElement(By.linkText("Log in")).click();
		driver.findElement(By.id("Email")).sendKeys("swathivenkat@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("swathi123");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		
		WebElement text = driver.findElement(By.linkText("swathivenkat@gmail.com"));
		System.out.println(text);
		
		WebElement computer=driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Computers')]"));
		Actions ac=new Actions(driver);
		ac.moveToElement(computer).build().perform();
		ac.moveToElement(driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Desktops')]"))).click().perform();
		WebElement e1=driver.findElement(By.id("products-orderby"));
		Select sc=new Select(e1);
		sc.selectByVisibleText("Price: Low to High");
		driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
		driver.findElement(By.id("add-to-cart-button-72")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Shopping cart')]")).click();
		driver.findElement(By.id("termsofservice")).click();
		driver.findElement(By.id("checkout")).click();
//		WebElement e2 = driver.findElement(By.id("BillingNewAddress_CountryId"));
//		Select sel=new Select(e2);
//		sel.selectByVisibleText("India");
//		driver.findElement(By.id("BillingNewAddress_City")).sendKeys("Chennai");
//		driver.findElement(By.id("BillingNewAddress_Address1")).sendKeys("RajaStreet");
//		driver.findElement(By.id("BillingNewAddress_ZipPostalCode")).sendKeys("601206");
//		driver.findElement(By.id("BillingNewAddress_PhoneNumber")).sendKeys("1425135278");
		Thread.sleep(7000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[1]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[2]")).click();	
		Thread.sleep(4000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[3]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[4]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("(//input[@value='Continue'])[5]")).click();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//input[@value='Confirm']")).click();
		Thread.sleep(4000);
	WebElement txt = driver.findElement(By.xpath("//*[contains(text(),'Your order has been successfully processed!')]"));
		System.out.println(txt.getText());
		driver.findElement(By.xpath("//a[contains(text(),'Log out')]")).click();
		  driver.close();

		
		
	}

}
